﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SimpleAnimation.Animation;

namespace SimpleAnimation
{
    public class Character
    {
        private bool _isMoving;
        private bool _isRunning;
        private double _animationElapsed;
        private Rectangle _animationFrame;
        private CharacterAnimation _characterAnimation;
        private float _velocity;
        private ActionType _currentAction = ActionType.Idle;

        public delegate void MovingHandler(Character sender, DisplacementArgs ca);


        public event MovingHandler Moving;

        private Texture2D _characterTexture;

        public Character(Texture2D characterTexture)
        {
            _velocity = 120;
            _characterTexture = characterTexture;
            _animationFrame = new Rectangle(0, 64, 24, 32);
        }
        public Character()
        {
            _velocity = 120;
            _animationFrame = new Rectangle(0, 64, 24, 32);
        }



        public bool IsMoving
        {
            get { return _isMoving; }
            set 
            {
                if (value != _isMoving && !value)
                {
                    _characterAnimation.Idle();
                }
                _isMoving = value; 
            }
        }
        
        public bool IsRunning
        {
            get { return _isRunning; }
            set { _isRunning = value; }
        }

        public float Velocity
        {
            get 
            {
                if (!_isRunning)
                    return _velocity;
                else
                    return _velocity * 2;
            }
            set { _velocity = value; }
        }

        public Texture2D CharacterTexture
        {
            get { return _characterTexture; }
            set { _characterTexture = value; }
        }

        public CharacterAnimation CharacterAnimation
        {
            get
            {
                return _characterAnimation;
            }
            set
            {
                _characterAnimation = value;
            }
        }

        public void UpdateAnimation(double elapsedTime)
        {
            /// **Update Me**
            /// Needs to be updated when implementing other action types
            if (_isMoving)
            { 
                _characterAnimation.Animate(_currentAction, elapsedTime);
                _animationElapsed += elapsedTime;

                //if (_animationElapsed > .2 / (_velocity / 120))
                //{
                //    _animationFrame.X += 24;

                //    if (_animationFrame.X > 48)
                //    {
                //        _animationFrame.X = 0;
                //    }
                //    _animationElapsed = 0;
                //}
            }
            else
            {
                //_animationFrame.X = 24;
            }
        }


        public void Move(Direction direction, double elapsedTime)
        {
            switch (direction)
            {
                case Direction.North:
                    _currentAction = ActionType.MoveUp;
                    break;
                case Direction.South:
                    _currentAction = ActionType.MoveDown;
                    break;
                case Direction.East:
                    _currentAction = ActionType.MoveRight;
                    break;
                case Direction.West:
                    _currentAction = ActionType.MoveLeft;
                    break;
            }
            _isMoving = true;
            Moving(this, new DisplacementArgs(direction, (Velocity * (float)elapsedTime)));
        }
    }
}
