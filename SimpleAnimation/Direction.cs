﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation
{
    public enum Direction
    {
        North = 1,
        South = 2,
        East = 3,
        West = 4,
        NothEast = 5,
        NorthWest = 6,
        SouthEast = 7,
        SouthWest = 8
    }
}
