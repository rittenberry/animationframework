﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace SimpleAnimation.Animation
{
    public abstract class AnimationBase
    {
        protected int _framesPerSecond;
        protected int _sizeX;
        protected int _sizeY;
  
        public virtual int FramesPerSecond
        {
            get { return _framesPerSecond; }
            set { _framesPerSecond = value; }
        }
        public virtual int SizeX
        {
            get { return _sizeX; }
            set { _sizeX = value; }
        }
        public virtual int SizeY
        {
            get { return _sizeY; }
            set { _sizeY = value; }
        }
    }
}
