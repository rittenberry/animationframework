﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SimpleAnimation.Animation
{
    public class AnimationFrame
    {
        protected float _duration;
        protected Rectangle _frame;

        public Rectangle Frame
        {
            get
            {
                return _frame;
            }
            set
            {
                _frame = value;
            }
        }

        public float Duration
        {
            get
            {
                return _duration;
            }
            set
            {
                _duration = value;
            }
        }

        public AnimationFrame()
        {
            Frame = new Rectangle();
        }

        public AnimationFrame(int x, int y, int height, int width)
        {
            Frame = new Rectangle(x, y, width, height);
        }
    }
}
