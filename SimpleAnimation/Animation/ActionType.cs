﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation.Animation
{
    public enum ActionType
    {
        MoveUp = 1,
        MoveRight = 2,
        MoveDown = 3,
        MoveLeft = 4,
        Idle,
        Action1,
        Action2
    }
}
