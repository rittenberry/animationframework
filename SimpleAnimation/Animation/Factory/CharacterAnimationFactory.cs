﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation.Animation.Factory
{
    public class CharacterAnimationFactory
    {
        public static CharacterAnimation Create()
        {
            CharacterAnimation ca = new CharacterAnimation();
            ca.AnimationGroups = new Dictionary<ActionType, CharacterAnimationGroup>();
            ca.AnimationGroups.Add(ActionType.MoveUp, AnimationGroupFactory.Create(0,0, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));
            ca.AnimationGroups.Add(ActionType.MoveRight, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));
            ca.AnimationGroups.Add(ActionType.MoveDown, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight * 2, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));
            ca.AnimationGroups.Add(ActionType.MoveLeft, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight * 3, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));

            ca.CurrentAnimationGroup = ca.AnimationGroups[ActionType.MoveDown];
            ca.FramesPerSecond = 3;
            
            return ca;
        }
    }
}