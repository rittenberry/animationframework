﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation.Animation.Factory
{
    public class AnimationGroupFactory
    {
        public static CharacterAnimationGroup Create(int x, int y,int frameWidth, int frameHeight, int numberOfFrames, int idleFrame)
        {
            CharacterAnimationGroup animationGroup = new CharacterAnimationGroup();

            for (int i = 0; i < numberOfFrames; i++)
            {
                AnimationFrame frame = new AnimationFrame(x + i * frameWidth, y, frameHeight, frameWidth);
                animationGroup.AnimationFrames.Add(frame);
                if (i == idleFrame)
                {
                    animationGroup.IdleFrame = frame;
                }
            }

            animationGroup.CurrentFrame = animationGroup.IdleFrame;

            return animationGroup;
        }
    }
}
