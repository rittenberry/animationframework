﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation
{
    public class DisplacementArgs : EventArgs
    {

        public Direction Direction
        {
            get;
            set;
        }

        public float Magnitude
        {
            get;
            set;
        }


        public DisplacementArgs()
        {
            
        }
        public DisplacementArgs(Direction direction, float magnitude)
        {
            Direction = direction;
            Magnitude = magnitude;
        }
    }
}
