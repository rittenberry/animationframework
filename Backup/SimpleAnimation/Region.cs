﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SimpleAnimation
{
    public class Region
    {
        Dictionary<Character, Vector2> _characters;

        public Dictionary<Character, Vector2> Characters
        {
            get { return _characters; }
            //set { _characters = value; }
        }

        public Region()
        {
            _characters = new Dictionary<Character, Vector2>();
        }

        public void AddCharacterToRegion(Character character, Vector2 startingLocation)
        {
            character.Moving += character_Moving;
            _characters.Add(character, startingLocation);
        }

        void character_Moving(Character sender, DisplacementArgs e)
        {
            Vector2 position = Characters[sender];

            switch (e.Direction)
            {
                case Direction.North:
                    position.Y -= e.Magnitude;
                    break;
                case Direction.South:
                    position.Y += e.Magnitude;
                    break;
                case Direction.East:
                    position.X += e.Magnitude;
                    break;
                case Direction.West:
                    position.X -= e.Magnitude;
                    break;
            }
            Characters[sender] = position;
        }
    }
}
