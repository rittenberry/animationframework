﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SimpleAnimation.Animation
{
    public class CharacterAnimation : AnimationBase
    {
        protected Texture2D animationMap;

        Dictionary<ActionType, CharacterAnimationGroup> _animationGroups;
        public Dictionary<ActionType, CharacterAnimationGroup> AnimationGroups
        {
            get { return _animationGroups; }
            set { _animationGroups = value; }
        }

        protected ActionType _currentAction;
        protected CharacterAnimationGroup _currentAnimationGroup;

        public CharacterAnimationGroup CurrentAnimationGroup
        {
            get { return _currentAnimationGroup; }
            set { _currentAnimationGroup = value; }
        }

        public CharacterAnimation()
        {

        }

        public Rectangle AnimationFrame
        {
            get { return CurrentAnimationGroup.CurrentFrame.Frame; }
        }

        public Texture2D AnimationMap
        {
            get { return animationMap; }
            set { animationMap = value; }
        }


        public void Animate(ActionType action, double elapsedTime)
        {
            if (action != _currentAction)
            {
                _currentAnimationGroup.ResetAnimation();
                _currentAction = action;
                _currentAnimationGroup = _animationGroups[_currentAction];
            }

            _currentAnimationGroup.Animate(elapsedTime);
        }

        public void Idle()
        {
            _currentAnimationGroup.Idle();
        }
    }
}
