﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleAnimation.Animation
{
    public class CharacterAnimationGroup
    {
        protected List<AnimationFrame> _animationFrames;
        protected AnimationFrame _currentFrame;
        protected AnimationFrame _idleFrame;
        protected double _elapsedTime;
        protected int _frameWidth;
        protected int _frameHeight;
        protected int _defaultDuration;

        public CharacterAnimationGroup()
        {
            _animationFrames = new List<AnimationFrame>();
        }

        public List<AnimationFrame> AnimationFrames
        {
            get { return _animationFrames; }
            set { _animationFrames = value; }
        }

        public int FrameCount
        {
            get
            {
                return _animationFrames.Count();
            }
        }

        public float CurrentFrameDuration
        {
            get
            {
                if (CurrentFrame.Duration != default(float))
                {
                    return _defaultDuration;
                }
                else
                {
                    return CurrentFrame.Duration;
                }
            }
        }

        public void Animate(double elapsedTime)
        {
            _elapsedTime += elapsedTime;
            if (ElapsedTime > CurrentFrameDuration)
            {
                ProgressAnimation();
            }
        }

        public void ResetAnimation()
        {
            _elapsedTime = 0;
        }

        public void ProgressAnimation()
        {
            int currentFrameIndex;
            currentFrameIndex = _animationFrames.IndexOf(_currentFrame);
            currentFrameIndex++;
            if (currentFrameIndex >= FrameCount)
            {
                currentFrameIndex = 0;
            }
            _currentFrame = _animationFrames[currentFrameIndex];
            _elapsedTime = 0;
        }
        
        public AnimationFrame IdleFrame
        {
            get { return _idleFrame; }
            set { _idleFrame = value; }
        }

        public AnimationFrame CurrentFrame
        {
            get
            {
                return _currentFrame;
            }
            set
            {
                _currentFrame = value;
            }
        }

        public double ElapsedTime
        {
            get
            {
                return _elapsedTime;
            
            }
            set
            {
                _elapsedTime = value;
            }
        }


        internal void Idle()
        {
            CurrentFrame = IdleFrame;
            _elapsedTime = 0;
        }
    }
}
